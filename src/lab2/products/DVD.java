package lab2.products;

public class DVD extends Product {
	
	private int duration;
	
	public DVD (String name, String description, int duration) {
	    super.name = name;
	    super.description = description;
	    this.duration = duration;
	}

}