package lab2.products;

public class Book extends Product {
	
	private int pageNumber;
	
	public Book(String name, String description, int pageNumber) {
		super.name = name;
		super.description = description;
		this.pageNumber = pageNumber;
	}
	
}