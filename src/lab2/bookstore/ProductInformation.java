package lab2.bookstore;

import lab2.products.Product;

public class ProductInformation {
	
	private Product product;
	private int price;
	private int numberOfItemsAvailable;

        /* Used to keep a "count" of items in the cart. */
	public ProductInformation(int numberOfItemsAvailable) {
	        this.numberOfItemsAvailable = numberOfItemsAvailable;
        }
	
	public ProductInformation(Product product, int price, int numberOfItemsAvailable){
		this.product = product;
		this.price = price;
		this.numberOfItemsAvailable = numberOfItemsAvailable;
	}
	
	public int getNumberOfItemsAvailable() {
		return this.numberOfItemsAvailable; /* Returns the available number of items */
	}
	
	public void setNumberOfItemsAvailable(int numberOfItemsAvailable) {
		this.numberOfItemsAvailable = numberOfItemsAvailable;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public Product getProduct() {
		return this.product;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((this.product.getDescription() == null) ? 0 : this.product.getDescription().hashCode());
		result = prime * result + ((this.product.getName() == null) ? 0 : this.product.getName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductInformation other = (ProductInformation) obj;
		if (this.product.getDescription() == null) {
			if (other.getProduct().getDescription() != null)
				return false;
		} else if (!this.product.getDescription().equals(other.getProduct().getDescription()))
			return false;
		if (this.product.getName() == null) {
			if (other.getProduct().getName() != null)
				return false;
		} else if (!this.product.getName().equals(other.getProduct().getName()))
			return false;
		return true;
	}
	
	@Override
	public ProductInformation clone() {
        try {
            return (ProductInformation) super.clone();
        } catch (CloneNotSupportedException e) {        
            return null;
        }
	}
	
}
