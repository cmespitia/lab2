package lab2.bookstore;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.Iterator;

import lab2.products.Product;

import java.util.HashSet;

public class Frontend implements IFrontend {
	
    Store store;

    private HashMap<Product, ProductInformation> inventory;
	
    public Frontend(Store store) {
    	this.store = store;	
    }
    
	public void addToCart(Product product, Cart cart) {
	        cart.addItem(product);

                /* Check if the product is already in the database. */
                if (!this.store.inventory.containsKey(product))
                        throw new BookstoreException("[Backend.add] This product does not exist in the DB.");
                else {
                        /* If the product is not in the database, create it and insert */
                        ProductInformation p = this.inventory.get(product);
                        int inventory = p.getNumberOfItemsAvailable() + 1; /* Get and increase inventory */
                        p.setNumberOfItemsAvailable(inventory);
			this.invetory.put(product, pi);   
                }

	}
	
	public void removeFromCart(Product product, Cart cart) {
		cart.removeItem(product);
	}
	
	public Set<ProductInformation> searchProduct (String nameOfProduct) {
		//Set<ProductInformation> s = new Set();

		Set<ProductInformation> s = new HashSet(this.store.database.values());
			
		
	    Iterator<ProductInformation> it = s.iterator();
		
		while (it.hasNext()) {
			ProductInformation val = (ProductInformation)it.next();
				if (!val.getProduct().getName().equals(nameOfProduct))
				   it.remove();
		}
		
		
		//s.retainAll(Arrays.asList(nameOfProduct));
		
		return s;
		
	}
	
	public void checkout (String name, String addressOfDelivery, CreditCard card, Cart cart) 
	throws BookstoreException {

		
		
		
	}
	
	public int totalPrice (Cart cart) {
		return cart.getTotalPrice();
	}
	
	public List<Product> getCartContent(Cart cart) {
		//return cart.getContent().clone().;
		return Arrays.asList(cart.getContent());
	}

}
