package lab2.bookstore;

import lab2.products.Product;

public class Backend implements IBackend {
	
	private Store store;
	//private HashMap<Product, ProductInformation> database;
	
	public Backend(Store store) {
		this.store = store;
	}
	
	public void addInformation(Product product, int price) throws BookstoreException {
		
	     /* Check if the product is already in the database. */
		if (this.store.database.containsKey(product))
			throw new BookstoreException("[Backend.addInformation] This product already exists in the DB.");
		else {	
			ProductInformation pi = new ProductInformation(product, price, 0);
			this.store.database.put(product, pi);
		}
		
	}
	
	public void add(Product product) throws BookstoreException {
		
	     /* Check if the product is already in the database. */
		if (!this.store.database.containsKey(product))
			throw new BookstoreException("[Backend.add] This product does not exist in the DB.");
		else {
			/* If the product is not in the database, create it and insert */
			ProductInformation p = this.store.database.get(product);
			int inventory = p.getNumberOfItemsAvailable() + 1; /* Get and increase inventory */
			p.setNumberOfItemsAvailable(inventory);
		
		}
		
	}
	
	public void remove(Product product) throws BookstoreException {
		
	     /* Check if the product is already in the database. */
		if (!this.store.database.containsKey(product))
			throw new BookstoreException("[Backend.remove] The product does not exist in the database.");
		else {
			/* If the product is not in the database, create it and insert */
			ProductInformation p = this.store.database.get(product);
			
			/* Check if the product is available. */
			int inventory = p.getNumberOfItemsAvailable();
			
			if (inventory > 0)
				p.setNumberOfItemsAvailable(inventory - 1);
			else
				throw new BookstoreException("[Backend.remove] The product is no longer in stock.");

		}
		
	}
	
	public ProductInformation lookup(Product product) {
		
		
		if (this.store.database.containsKey(product)) {
			
			    ProductInformation result = this.store.database.get(product);

				return new ProductInformation(result.getProduct(), result.getPrice(), result.getNumberOfItemsAvailable());
					
			    
		} else
			return null;
	  /*
		Iterator<Product> it = this.database.keySet().iterator();
		
		while (it.hasNext()) {
			Product key = (Product)it.next();
			ProductInformation val = (ProductInformation)this.database.get(key);
			
			if (key.hashCode() == product.hashCode())
				return val;
			
		}
		
		return null;
		*/
		
	}
	
}
